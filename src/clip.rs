use super::types::{Surface,Rect};

pub struct Clip<'a, S> {
    surf: &'a mut S,
    rect: Rect
}

impl<'a, S: Surface> Clip<'a, S> {
    pub fn new(surf: &'a mut S, rect: &Rect) -> Self {
        Self { surf, rect: rect.clone() }
    }
}

impl<'a, S: Surface> Surface for Clip<'a, S> {
    type Item = S::Item;
    fn width(&self) -> usize { self.rect.w }
    fn height(&self) -> usize { self.rect.h }
    fn stride(&self) -> usize {  self.surf.stride()  }
    fn as_slice(&self) -> &[Self::Item] {
        // TODO: intersections
        let s = self.surf.stride();
        let x = self.rect.x as usize;
        let y = self.rect.y as usize;
        let _w = self.rect.w;
        let h = self.rect.h;
        let o = y * s + x;
        let oo = (y + h) * s;
        let d = self.surf.as_slice();
        &d[o..oo]
    }
    fn as_mut_slice(&mut self) -> &mut [Self::Item] {
        // TODO: intersections
        let s = self.surf.stride();
        let x = self.rect.x as usize;
        let y = self.rect.y as usize;
        let _w = self.rect.w;
        let h = self.rect.h;
        let o = y * s + x;
        let oo = (y + h) * s;
        let d = self.surf.as_mut_slice();
        &mut d[o..oo]
    }
}
