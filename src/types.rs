
#[derive(Clone)]
pub struct Point {
    pub x: isize,
    pub y: isize,
}

#[derive(Clone)]
pub struct Rect {
    pub x: isize,
    pub y: isize,
    pub w: usize,
    pub h: usize,
}

impl Rect {
    pub fn right(&self) -> isize {
        if self.w > 1 {
            self.x + self.w as isize - 1
        }
        else {
            self.x
        }
    }
    pub fn bottom(&self) -> isize {
        if self.h > 1 {
            self.y + self.h as isize - 1
        }
        else {
            self.y
        }
    }
}

pub trait Surface {
    type Item;
    fn width(&self) -> usize { 0 }
    fn height(&self) -> usize { 0 }
    fn stride(&self) -> usize {  self.width()  }
    fn as_slice(&self) -> &[Self::Item] {  &[]  }
    fn as_mut_slice(&mut self) -> &mut [Self::Item] {  &mut[]  }
}

pub trait BitFont {
    fn glyph_width(&self, c: u32) -> usize;
    fn glyph_height(&self, c: u32) -> usize;
    fn glyph_bits(&self, c: u32) -> &[u8];
}
