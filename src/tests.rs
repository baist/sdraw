use super::*;

struct Surf {
    data: [u8; 100]
}

impl Surf {
    fn new() -> Self { Self { data: [0; 100] } }
}

impl Surface for Surf {
    type Item = u8;
    fn width(&self) -> usize { 10 }
    fn height(&self) -> usize { 10 }
    fn stride(&self) -> usize {  self.width()  }
    fn as_slice(&self) -> &[Self::Item] {  &self.data  }
    fn as_mut_slice(&mut self) -> &mut [Self::Item] {  &mut self.data  }
}

#[test]
fn test_fill()
{
    let mut surf = Surf::new();
    let mut draw = Draw::new(&mut surf);
    draw.fill(16u8);
    draw.fill(0u8);
}

#[test]
fn test_point()
{
    let mut surf = Surf::new();
    let mut draw = Draw::new(&mut surf);
    draw.point(&Point{x:0,y:0}, 16u8);
    draw.point(&Point{x:9,y:9}, 16u8);
    draw.point(&Point{x:10,y:10}, 16u8);
    draw.point(&Point{x:-1,y:-1}, 16u8);
}

#[test]
fn test_hline()
{
    let mut surf = Surf::new();
    let mut draw = Draw::new(&mut surf);
    draw.hline(&Point{x:0,y:0}, 5, 16u8);
    draw.hline(&Point{x:-1,y:0}, 5, 16u8);
    draw.hline(&Point{x:-1,y:-1}, 5, 16u8);
    draw.hline(&Point{x:-1,y:10}, 5, 16u8);
    draw.hline(&Point{x:10,y:0}, 5, 16u8);
    draw.hline(&Point{x:11,y:0}, 15, 16u8);
}

#[test]
fn test_vline()
{
    let mut surf = Surf { data: [0; 100] };
    let mut draw = Draw::new(&mut surf);
    draw.vline(&Point{x:0,y:0}, 5, 16u8);
    draw.vline(&Point{x:5,y:-1}, 5, 16u8);
    draw.vline(&Point{x:5,y:-2}, 11, 16u8);
    draw.vline(&Point{x:-1,y:10}, 5, 16u8);
    draw.vline(&Point{x:10,y:0}, 5, 16u8);
    draw.vline(&Point{x:0,y:11}, 15, 16u8);
}

#[test]
fn test_frame()
{
    let mut surf = Surf { data: [0; 100] };
    let mut draw = Draw::new(&mut surf);
    draw.frame(&Rect{x:-2,y:-2,w:4,h:4}, 16u8);
    draw.frame(&Rect{x:0,y:0,w:4,h:4}, 16u8);
    draw.frame(&Rect{x:8,y:8,w:4,h:4}, 16u8);
    draw.frame(&Rect{x:11,y:11,w:4,h:4}, 16u8);
    draw.frame(&Rect{x:11,y:11,w:0,h:4}, 16u8);
}

#[test]
fn test_rect()
{
    let mut surf = Surf { data: [0; 100] };
    let mut draw = Draw::new(&mut surf);
    draw.rect(&Rect{x:-2,y:-2,w:4,h:4}, 16u8);
    draw.rect(&Rect{x:0,y:0,w:4,h:4}, 16u8);
    draw.rect(&Rect{x:8,y:8,w:4,h:4}, 16u8);
    draw.rect(&Rect{x:11,y:11,w:4,h:4}, 16u8);
    draw.rect(&Rect{x:11,y:11,w:4,h:0}, 16u8);
}
