use core::mem::swap;
use core::marker::PhantomData;

use super::types::{Surface,Point,Rect,BitFont};

// Define region codes
const INSIDE: u8 = 0b0000;
const LEFT: u8   = 0b0001;
const RIGHT: u8  = 0b0010;
const BOTTOM: u8 = 0b0100;
const TOP: u8    = 0b1000;

// Function to calculate region code for a point
// x, y and (x_min, y_min, x_max, y_max)
fn calc_region_code(x: isize, y: isize, reg: (isize,isize,isize,isize)) -> u8
{
    let mut code = INSIDE;
    if x < reg.0 {
        code |= LEFT;
    } else if x > reg.2 {
        code |= RIGHT;
    }
    if y < reg.1 {
        code |= BOTTOM;
    } else if y > reg.3 {
        code |= TOP;
    }
    code
}

// Function to perform Cohen-Sutherland line clipping
fn line_clip(mut x1: isize, mut y1: isize, mut x2: isize, mut y2: isize,
    reg: (isize,isize,isize,isize)) -> Option<(isize,isize,isize,isize)>
{
    let mut code1 = calc_region_code(x1, y1, reg);
    let mut code2 = calc_region_code(x2, y2, reg);
    let mut accept = false;

    loop {
        if code1 | code2 == INSIDE {
            // both points inside window
            accept = true;
            break;
        }
        else if code1 & code2 != INSIDE {
            break;
        }
        let code_outside = if code2 > code1 { code2 } else { code1 };
        let x: isize;
        let y: isize;
        if code_outside & TOP != 0 {
            // Point is above the viewport
            x = x1 + (x2 - x1) * (reg.3 - y1) / (y2 - y1);
            y = reg.3;
        } else if code_outside & BOTTOM != 0 {
            // Point is below the viewport
            x = x1 + (x2 - x1) * (reg.1 - y1) / (y2 - y1);
            y = reg.1;
        } else if code_outside & RIGHT != 0 {
            // Point is to the right of the viewport
            y = y1 + (y2 - y1) * (reg.2 - x1) / (x2 - x1);
            x = reg.2;
        } else { // if code_outside & LEFT != 0 {
            // Point is to the left of the viewport
            y = y1 + (y2 - y1) * (reg.0 - x1) / (x2 - x1);
            x = reg.0;
        }
        if code_outside == code1 {
            x1 = x;
            y1 = y;
            code1 = calc_region_code(x1, y1, reg);
        } else {
            x2 = x;
            y2 = y;
            code2 = calc_region_code(x2, y2, reg);
        }
    }

    if accept { Some((x1, y1, x2, y2)) }
    else { None }
}

fn intersect(first: &Rect, second: &Rect) -> Option<Rect>
{
    if first.w == 0 || first.h == 0 ||
        second.w == 0 || second.h == 0
    {
        return None;
    }
    let x_overlap = (first.x..first.x + first.w as isize).contains(&second.x)
        || (second.x..second.x + second.w as isize).contains(&first.x);

    let y_overlap = (first.y..first.y + first.h as isize).contains(&second.y)
        || (second.y..second.y + second.h as isize).contains(&first.y);

    if x_overlap && y_overlap {
        let x = first.x.max(second.x);
        let y = first.y.max(second.y);
        let w = ((first.x + first.w as isize).min(second.x + second.w as isize) - x) as usize;
        let h = ((first.y + first.h as isize).min(second.y + second.h as isize) - y) as usize;

        Some(Rect { x, y, w, h })
    } else {
        None
    }
}

pub struct Draw<'a, S, C> {
    surf: &'a mut S,
    phantom: PhantomData<C>,
}

impl<'a, S: Surface<Item = C>, C: Clone> Draw<'a, S, C> {
    pub fn new(surf: &'a mut S) -> Self {
        Self { surf, phantom: PhantomData }
    }

    pub fn fill(&mut self, color: C) {
        let strd = self.surf.stride();
        let w = self.surf.width();
        let h = self.surf.height();
        let data = self.surf.as_mut_slice();
        let col: C = color.into();
        let mut o = 0;
        for _ in 0..h {
            let e = o + w;
            let rng = o..e;
            for d in &mut data[rng] {  *d = col.clone();  }
            o += strd;
        }
    }

    pub fn point(&mut self, p: &Point, color: C) {
        let mut res = (p.x >= 0) & (p.y >= 0);
        res &= (p.x < self.surf.width() as isize) & (p.y < self.surf.height() as isize);
        if !res { return; }
        let strd = self.surf.stride();
        let s = self.surf.as_mut_slice();
        let col: C = color.into();
        let o = p.y as usize * strd + p.x as usize;
        s[o] = col;
    }

    pub fn hline(&mut self, p: &Point, x: isize, color: C) {
        let right = self.surf.width() as isize - 1;
        let mut res = (0 <= p.y) & (p.y < self.surf.height() as isize);
        res &= p.x >= 0 || x >= 0;
        res &= p.x <= right || x <= right;
        if !res { return; }
        let mut s_x = p.x.max(0).min(right);
        let mut e_x = x.max(0).min(right);
        if s_x > e_x { swap(&mut s_x, &mut e_x); }
        let w = ((e_x - s_x) + 1) as usize;
        self.hline_(&Point{x:s_x,y:p.y}, w, color);
    }

    pub fn vline(&mut self, p: &Point, y: isize, color: C) {
        let bottom = self.surf.height() as isize - 1;
        let mut res = (0 <= p.x) & (p.x < self.surf.width() as isize);
        res &= p.y >= 0 || y >= 0;
        res &= p.y <= bottom || y <= bottom;
        if !res { return; }
        let mut s_y = p.y.max(0).min(bottom);
        let mut e_y = y.max(0).min(bottom);
        if s_y > e_y { swap(&mut s_y, &mut e_y); }
        let h = ((e_y - s_y) + 1) as usize;
        self.vline_(&Point{x:p.x,y:s_y}, h, color);
    }

    pub fn line(&mut self, a: &Point, b: &Point, color: C) {
        let right = self.surf.width() as isize - 1;
        let bottom = self.surf.height() as isize - 1;
        if let Some(l) = line_clip(a.x, a.y, b.x, b.y, (0,0,right,bottom)) {
            self.line_(&Point{x:l.0,y:l.1}, &Point{x:l.2,y:l.3}, color);
        }
    }

    pub fn frame(&mut self, r: &Rect, color: C) {
        if r.w == 0 || r.h == 0 { return; }
        self.frame_(&r, color);
    }

    pub fn rect(&mut self, r: &Rect, color: C) {
        let rect1 = Rect{x:0,y:0,w:self.surf.width(),h:self.surf.height()};
        if let Some(intersection) = intersect(&rect1, &r) {
            self.rect_(&intersection, color);
        }
    }

    pub fn circle(&mut self, center: &Point, radius: isize, color: C) {
        if (center.x - radius) < 0 || (center.y - radius) < 0 ||
            (center.x + radius) as usize >= self.surf.width() ||
            (center.y + radius) as usize >= self.surf.height()
        {
            return
        }
        let strd = self.surf.stride();
        let data = self.surf.as_mut_slice();
        let col: C = color.into();
        let mut o0 = (center.y - radius) as usize * strd + center.x as usize;
        let mut o1 = o0;
        let mut o2 = center.y as usize * strd + (center.x - radius) as usize;
        let mut o3 = o2 + (radius + radius) as usize;
        let mut o4 = center.y as usize * strd + (center.x - radius) as usize;
        let mut o5 = o4 + (radius + radius) as usize;
        let mut o6 = (center.y + radius) as usize * strd + center.x as usize;
        let mut o7 = o6;
        let mut d = (5 - radius * 4) / 4;
        let mut x = 0;
        let mut y = radius;

        while x < y {
            if d < 0 {
                d = d + x + x + 1;
            } else {
                d = d + (x - y) + (x - y) + 1;
                o0 += strd;
                o1 += strd;
                o2 += 1;
                o3 -= 1;
                o4 += 1;
                o5 -= 1;
                o6 -= strd;
                o7 -= strd;
                y -= 1;
            }
            data[o0] = col.clone();
            data[o1] = col.clone();
            data[o2] = col.clone();
            data[o3] = col.clone();
            data[o4] = col.clone();
            data[o5] = col.clone();
            data[o6] = col.clone();
            data[o7] = col.clone();
            o0 -= 1;
            o1 += 1;
            o2 -= strd;
            o3 -= strd;
            o4 += strd;
            o5 += strd;
            o6 -= 1;
            o7 += 1;
            x += 1;
        }
    }

    pub fn disc(&mut self, center: &Point, radius: isize, color: C) {
        if (center.x - radius) < 0 || (center.y - radius) < 0 ||
            (center.x + radius) as usize >= self.surf.width() ||
            (center.y + radius) as usize >= self.surf.height()
        {
            return
        }
        let strd = self.surf.stride();
        let data = self.surf.as_mut_slice();
        let col: C = color.into();
        let mut o0 = (center.y - radius) as usize * strd + center.x as usize;
        let mut o1 = o0;
        let mut o2 = center.y as usize * strd + (center.x - radius) as usize;
        let mut o3 = o2 + (radius + radius) as usize;
        let mut o4 = center.y as usize * strd + (center.x - radius) as usize;
        let mut o5 = o4 + (radius + radius) as usize;
        let mut o6 = (center.y + radius) as usize * strd + center.x as usize;
        let mut o7 = o6;
        let mut d = (5 - radius * 4) / 4;
        let mut x = 0;
        let mut y = radius;

        while x < y {
            if d < 0 {
                d = d + x + x + 1;
            } else {
                d = d + (x - y) + (x - y) + 1;
                for o in o0..=o1 {
                    data[o] = col.clone();
                }
                for o in o6..=o7 {
                    data[o] = col.clone();
                }
                o0 += strd;
                o1 += strd;
                o2 += 1;
                o3 -= 1;
                o4 += 1;
                o5 -= 1;
                o6 -= strd;
                o7 -= strd;
                y -= 1;
            }
            for o in o2..=o3 {
                data[o] = col.clone();
            }
            for o in o4..=o5 {
                data[o] = col.clone();
            }
            o0 -= 1;
            o1 += 1;
            o2 -= strd;
            o3 -= strd;
            o4 += strd;
            o5 += strd;
            o6 -= 1;
            o7 += 1;
            x += 1;
        }
    }

    pub fn text<F: BitFont>(&mut self, p: &Point, txt: &str, fnt: &F, color: C)
    {
        let mut p = p.clone();
        if p.x >= self.surf.width() as isize ||
            p.y >= self.surf.height() as isize {
            return;
        }

        let wdth = self.surf.width();
        let hght = self.surf.height();
        let strd = self.surf.stride();
        let data = self.surf.as_mut_slice();

        for c in txt.chars() {
            let u = u32::from(c);
            let w = fnt.glyph_width(u);
            let h = fnt.glyph_height(u);
            let b = fnt.glyph_bits(u);
            // margins
            let marg_left = if p.x < 0 { p.x.abs() } else { 0 };
            let marg_top = if p.y < 0 { p.y.abs() } else { 0 };
            let marg_right = p.x + w as isize - wdth as isize;
            let marg_right = if marg_right > 0 { marg_right } else { 0 };
            let marg_bottom = p.y + h as isize - hght as isize;
            let marg_bottom = if marg_bottom > 0 { marg_bottom } else { 0 };
            // clipped width and height
            let clip_w = (w as isize - (marg_left + marg_right)).max(0);
            let clip_h = (h as isize - (marg_top + marg_bottom)).max(0);
            let pad = strd - clip_w as usize;
            let pad1 = w - clip_w as usize;
            let mut o = p.y.max(0) as usize * strd + p.x.max(0) as usize;
            let mut i = marg_left as usize + marg_top as usize * w;
            for _row in 0..clip_h {
                for _col in 0..clip_w {
                    if b[i] == 1 {
                        data[o] = color.clone();
                    }
                    o += 1;
                    i += 1;
                }
                o += pad;
                i += pad1;
            }
            //
            // Update p.x for the next character, considering clipped width
            p.x += w as isize;
            if p.x >= wdth as isize {
                break;
            }
        }
    }

    fn hline_(&mut self, p: &Point, w: usize, color: C)
    {
        let strd = self.surf.stride();
        let s = self.surf.as_mut_slice();
        let mut o = p.y as usize * strd + p.x as usize;
        for _ in 0..w {
            s[o] = color.clone();
            o += 1;
        }
    }

    fn vline_(&mut self, p: &Point, h: usize, color: C)
    {
        let strd = self.surf.stride();
        let s = self.surf.as_mut_slice();
        let mut o = p.y as usize * strd + p.x as usize;
        for _ in 0..h {
            s[o] = color.clone();
            o += strd;
        }
    }


    fn line_(&mut self, a: &Point, b: &Point, col: C)
    {
        let strd = self.surf.stride() as isize;
        let p = self.surf.as_mut_slice();
        let a_x = a.x as isize;
        let a_y = a.y as isize;
        let b_x = b.x as isize;
        let b_y = b.y as isize;

        let c: C = col.into();

        let mut w = b_x - a_x;
        let mut h = b_y - a_y;

        let sx_;
        let sy_;

        if w < 0 {
            w = -w;
            sx_ = -1;
        }
        else {
            sx_ = 1;
        }

        if h < 0 {
            h = -h;
            sy_ = -strd;
        }
        else {
            sy_ = strd;
        }

        let dbl_w = w * 2;
        let dbl_h = h * 2;

        let d;
        let mut error;
        let dbl0;
        let dbl1;
        let sx0;
        let sx1;
        let sy0;
        let sy1;

        if w > h {
            d = w;
            dbl0 = dbl_w;
            dbl1 = dbl_h;
            sx0 = 0;
            sx1 = sx_;
            sy0 = sy_;
            sy1 = 0;
        }
        else {
            d = h;
            dbl0 = dbl_h;
            dbl1 = dbl_w;
            sx0 = sx_;
            sx1 = 0;
            sy0 = 0;
            sy1 = sy_;
        }

        error = dbl1 - d;

        let mut o = a_y * strd + a_x;

        for _ in 0..=d {
            p[o as usize] = c.clone();
            let mut x_ = sx1;
            let mut y_ = sy1;
            if error >= 0 {
                error -= dbl0;
                x_ += sx0;
                y_ += sy0;
            }
            o += x_ + y_;
            error += dbl1;
        }
    }

    fn frame_(&mut self, r: &Rect, color: C)
    {
        let col: C = color.into();
        let right_x = r.x + r.w as isize - 1;
        let bottom_y = r.y + r.h as isize - 1;
        self.hline(&Point{x:r.x,y:r.y},right_x,col.clone());
        self.hline(&Point{x:r.x,y:bottom_y},right_x,col.clone());
        self.vline(&Point{x:r.x,y:r.y},bottom_y,col.clone());
        self.vline(&Point{x:right_x,y:r.y},bottom_y,col);
    }

    fn rect_(&mut self, r: &Rect, col: C)
    {
        let strd = self.surf.stride();
        let data = self.surf.as_mut_slice();
        let c: C = col.into();
        let mut o = r.y as usize * strd + r.x as usize;
        for _ in 0..r.h {
            let e = o + r.w;
            let rng = o..e;
            for d in &mut data[rng] {  *d = c.clone();  }
            o += strd;
        }
    }
}
