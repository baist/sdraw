use sdraw::{Point,Rect,Draw,Surface};

struct Surf {
    data: [u8; 400]
}

impl Surf {
    fn new() -> Self { Self { data: [0; 400] } }
}

impl Surface for Surf {
    type Item = u8;
    fn width(&self) -> usize { 20 }
    fn height(&self) -> usize { 20 }
    fn stride(&self) -> usize {  self.width()  }
    fn as_slice(&self) -> &[Self::Item] {  &self.data  }
    fn as_mut_slice(&mut self) -> &mut [Self::Item] {  &mut self.data  }
}

fn print_surf(surf: &Surf)
{
    let s = surf.as_slice();
    let h = surf.height();
    let w = surf.width();
    let mut o = 0;
    for _ in 0..h {
        for _ in 0..w {
            let c = if s[o] == 0 { ".." } else { "XX" };
            print!("{}", c);
            o += 1;
        }
        println!("");
    }
}

fn main()
{
    let mut surf = Surf::new();
    let mut draw = Draw::new(&mut surf);
    draw.rect(&Rect{x:1,y:1,w:18,h:18}, 16u8);
    draw.fill(0u8);
    draw.rect(&Rect{x:1,y:1,w:18,h:0}, 16u8);
    draw.point(&Point{x:19,y:1}, 16u8);
    draw.hline(&Point{x:0,y:0}, 0, 16u8);
    draw.hline(&Point{x:2,y:0}, 3, 16u8);
    draw.hline(&Point{x:5,y:0}, 11, 16u8);
    draw.hline(&Point{x:18,y:3}, 22, 16u8);
    draw.hline(&Point{x:22,y:4}, 25, 16u8);
    draw.vline(&Point{x:0,y:2}, 2, 16u8);
    draw.vline(&Point{x:0,y:4}, 5, 16u8);
    draw.vline(&Point{x:0,y:6}, 11, 16u8);
    draw.vline(&Point{x:5,y:18}, 22, 16u8);
    draw.frame(&Rect{x:3,y:2,w:4,h:0}, 16u8);
    draw.frame(&Rect{x:3,y:3,w:4,h:1}, 16u8);
    draw.frame(&Rect{x:3,y:5,w:4,h:4}, 16u8);
    draw.frame(&Rect{x:8,y:5,w:1,h:4}, 16u8);
    draw.frame(&Rect{x:18,y:5,w:4,h:4}, 16u8);
    draw.frame(&Rect{x:-2,y:18,w:4,h:4}, 16u8);
    draw.rect(&Rect{x:10,y:10,w:4,h:4}, 16u8);
    draw.rect(&Rect{x:19,y:10,w:4,h:4}, 16u8);
    draw.rect(&Rect{x:-1,y:14,w:4,h:3}, 16u8);
    draw.line(&Point{x:3,y:10}, &Point{x:14,y:21}, 16u8);
    print_surf(&surf);
}